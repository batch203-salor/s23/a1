console.log("Using initilized object:");
const trainer = {
  name: "Ash Ketchum",
  age: 10,
  pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
  friends: {
    hoenn: ["May", "Max"],
    kanto: ["Brock", "Misty"],
  },
  talk: function (name) {
    this.name = name;
    console.log("Result of talk method");
    console.log(`${this.name}! I choose you!`);
  },
};

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);

trainer.talk("Pikachu");

console.log("Using with reinitilization:");

let trainerV2 = {
  name: "",
  age: Number,
  friends: {
    hoenn: [],
    kanto: [],
  },
  pokemon: [],
  talk: function (name) {
    this.name = name;
    console.log("Result of talk method");
    console.log(`${this.name}! I choose you!`);
  },
};
trainerV2.name = "Ash Ketchum";
trainerV2.age = 10;
trainerV2.friends.hoenn = ["May", "Max"];
trainerV2.friends.kanton = ["Brock", "Misty"];
trainerV2.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"];

console.log(trainerV2);

console.log("Result of dot notation:");
console.log(trainerV2.name);
console.log("Result of square bracket notation:");
console.log(trainerV2["pokemon"]);

trainerV2.talk("Pikachu");

function Pokemon(name, level){

	
		this.name = name;
		this.level = level;
		this.health = level * 2;
		this.attack = level * 1.5;

	
		this.tackle = function(target){
			console.log(this.name + " tackled " + target.name);

		

			
			target.health -= this.attack

			console.log(target.name + " health is now reduced to " + target.health);

			
			if(target.health <= 0){	
				
				target.faint()
			}
		}
		this.faint = function(){
			console.log(this.name + " fainted.");
		}

	}

	let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu);
 	
 	let geodude = new Pokemon("Geodude", 8);
	console.log(geodude);

	let mewtwo = new Pokemon("Mewtwo", 100);
	console.log(mewtwo);